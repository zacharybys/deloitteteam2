//Handler for Account Object
public with sharing class AccountHandler extends NF_AbstractTriggerHandler {
    
    
	public override void beforeUpdate(){
       
       // List of new contacts
        List<Contact> newContacts = new List<Contact>();
        
        // Iterate over modified accounts
        for (Account newAccount : (List<Account>)Trigger.new) {
            
            // Find associated contact and modify it
            Contact currentContact = [SELECT Name FROM Contact WHERE ExternalID__c =: newAccount.ID];
            currentContact.LastName = newAccount.Name;
            newContacts.add(currentContact);
                      
        }
        
        // Update all contacts in list
        update newContacts;
        
        
	}

	public override void afterUpdate(){

        
        

	}

	public override void beforeInsert(){
    
        
	}

	public override void afterInsert(){
        
        // List of new contacts
        List<Contact> newContacts = new List<Contact>();
        
        // Iterate over new accounts
        for (Account newAccount : (List<Account>)Trigger.new) {
            // Create and add new contact to list
            newContacts.add(new Contact(LastName = newAccount.Name, ExternalID__c = newAccount.Id));
                              
        }
        
        // Upsert all contacts in list
        upsert newContacts;
     
	}

	public override void afterDelete(){

	}

	public override void andFinally(){

	}
}