
SELECT contact_load.ID as CONTACTID, ACCOUNTID, LASTNAME, FIRSTNAME, SALUTATION, account_load.NAME as ACCOUNTNAME, contact_load.NAME as CONTACTNAME, MAILINGCITY, MAILINGSTATE, MAILINGPOSTALCODE, MAILINGCOUNTRY, contact_load.PHONE as CONTACTPHONE, account_load.PHONE as ACCOUNTPHONE, REPLACE ((REPLACE (MOBILEPHONE, '(','')), ')', '') as MOBILEPHONE, OTHERPHONE, ASSISTANTPHONE, EMAIL, TITLE, DEPARTMENT
FROM deloittehackathon.contact_load
JOIN deloittehackathon.account_load
	ON deloittehackathon.account_load.ID = deloittehackathon.contact_load.ACCOUNTID
		WHERE deloittehackathon.account_load.INDUSTRY != ''
			AND
				deloittehackathon.account_load.BILLINGCITY OR deloittehackathon.account_load.BILLINGPOSTALCODE!= ''
			AND
				deloittehackathon.contact_load.TITLE OR deloittehackathon.contact_load.DEPARTMENT !='';